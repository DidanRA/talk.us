<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_index extends CI_Model {

	private $table = "user";
	public $username;
	public $email;
	public $password;


	public function getAll($where){
		return $this->db->get_where($this->table,$where)->result();
	}
	function cek_login($where){		
		return $this->db->get_where($this->table,$where);
	}
	public function getId($where)
	{
		$this->db->select('id');
		$this->db->where($where);
		return $this->db->get($this->table);
	}

}

/* End of file  */
/* Location: ./application/models/ */