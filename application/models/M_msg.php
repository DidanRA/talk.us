<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_msg extends CI_Model {

	private $table = "message";
	public $sender;
	public $receiver;
	public $message;

	public function getAll($where)
	{
		return $this->db->get_where($this->table,$where)->result();
	}
	public function send($sender,$receiver){
		$post = $this->input->post();
        $this->sender = $sender;
        $this->receiver = $receiver;
        // $this->message = htmlspecialchars($post['msg']);
        if ($post['msg']== null) {
        	return;
        }
        else {
        	$this->message = htmlspecialchars($post['msg']);
        }
        $this->db->insert($this->table, $this);
	}

	

}

/* End of file M_msg.php */
/* Location: ./application/models/M_msg.php */