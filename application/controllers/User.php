<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct(){
		parent::__construct();
		$this->load->model('m_index');
		$this->load->model('m_msg');
	}
	public function index()
	{
		$this->load->view('sign');
		// $param = 15;
		// $where = "id != $param";
		// $get = $this->m_index;
		// $data['data'] = $get->getAll($where);
		// $this->load->view('dashboard', $data);
	}
	public function welcome()
	{
		$param = intval($this->session->userdata('id'));
		$where = "id != $param";
		$get = $this->m_index;
		$data['data'] = $get->getAll($where);
		$data['msg'] = "";
		$data['friend'] = "";
		$this->load->view('dashboard', $data);
		
	}
	public function getMsg($receiver,$sender){
		$param = intval($this->session->userdata('id'));
		$where_user = "id != $param";
		$where_msg = "(sender = $sender AND receiver = $receiver) OR (sender = $receiver AND receiver = $sender)";
		$getMsg = $this->m_msg;
		$getUser = $this->m_index;
		$data['msg'] = $getMsg->getAll($where_msg);
		$data['data'] = $getUser->getAll($where_user);
		$data['friend'] = $receiver;
		$this->load->view('dashboard', $data);

	}
	public function chat($sender,$receiver){
		$product = $this->m_msg;
		$product->send($sender,$receiver);
		
		$this->getMsg($receiver,$sender);
	}
	function login(){
		$user = $this->m_index;
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$where = array(
			'username' => $username,
			'password' => md5($password) 
			
		);
		$cek = $this->m_index->cek_login($where)->num_rows();
		if($cek == true){
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$where = array(
				'username' => $username,
				'password' => md5($password) 

			);
			$result=$user->getId($where)->row();
			$id=$result->id;
			// $data['data'] =$user->getById($where);
			$data_session = array(
				'id' => $id,
				'nama' => $username,
				'password' => $password,
				'status' => "online"
			);

			$this->session->set_userdata($data_session);
			
			redirect(base_url('user/welcome'));


		}else{
			echo "error";
		}	
	}
	function logout(){
		$this->session->sess_destroy();
		redirect(base_url(""));
	}
}
